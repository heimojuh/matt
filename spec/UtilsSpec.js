
describe("Utils", function() {

    
    it("Should exist", function() { 
   
        expect(window.utils).not.toBeNull();
    });

    it("Should update number by 0.01", function() { 
        var val;
        var future = window.utils.updateNumbers(10,11, function(n) {val = n;});
        waitsFor(function() { return val === 10.01;}, "upped by 0.01", 10000);
        runs(function() { 
            expect(val).toBe(10.01);
            future.clear();
        });

    });

    it("Updates value to target number", function() { 
        var val;
        var future = window.utils.updateNumbers(10,10.06, function(n) { val = n;});

        waitsFor(function() { return val >= 10.05; }, "upped to 11", 10000);
        runs(function() { 
            expect(val >= 10.05).toBeTruthy();
            future.clear();
        });
    });

    it("Runs callback when we pass treshold", function() { 
        var test = 0;
        var t = utils.checkTreshold(101, function () { test = 1;});
        expect(test).toBe(1);
        t.reset();
    });

    it("Does not run cb when under treshold", function() { 
        var test = 0;
        var t = utils.checkTreshold(99, function () { test = 1;});
        expect(test).toBe(0);
        t.reset();
    });
    
    it("It ups current treshold by treshold when over", function() { 
        var test = 0;
        var t = utils.checkTreshold(101, function () { test = 1;});
        expect(t.c).toBe(200);
        t.reset();
    });
    
    it("Does not up the treshold when under treshold", function() { 
        var test = 0;
        var t = utils.checkTreshold(99, function () { test = 1;});
        expect(t.c).toBe(100);
        t.reset();
    });

    it("formats 60 seconds like 0h 1m 0s", function() { 
        
        var t = utils.parseTime(60);
        expect(t).toBe("0h 1m 0s");
    
    });
    it("formats 50 seconds like 0h m 50s", function() { 
        
        var t = utils.parseTime(50);
        expect(t).toBe("0h 0m 50s");
    
    });


    it("formats 60*61+1 seconds like 1h 1m 1s", function() { 
        
        var t = utils.parseTime(60*61+1);
        expect(t).toBe("1h 1m 1s");
    
    });
});
