
                window.ns = (function($, utils) {
                "use strict";
                    var slogans = ["Delivering value", "Digital natives", "We learn and grow together", "Core network disturbance", "XyZ is available now!"];
                    var timer;
                    var sound = 0;
                    var sm = 633600
                    var sum = 0;
                    var seconds = 0;
                    var treshold = 100;
                    var future;

                    var start = new Date().getTime(); 
                    var time = 0;
                    var elapsed = '0.0'; 

                    function pickSlogan() { 

                        return slogans[Math.floor(Math.random()*slogans.length)];
                    }

                    function printSlogan() { 
                        var s = $("#slogan");
                        s.fadeOut("slow", function() { 
                            s.html(pickSlogan());
                            s.fadeIn("slow");
                        });

                    }

                    function round(value) { 
                        var m = Math.pow(10, 2);
                        return Math.round(value * m) / m;

                    }

                    function calcIndSums(salary, persons) { 

                        sum = sum + ((parseInt($(salary).val(), 10) * parseInt($(persons).val(), 10)) / (sm)) ;
                    }

                    function calculateSum() { 

                        return calcIndSums("#normalsalary", "#normalsalaryamount") + calcIndSums("#execsalary", "#execamount");

                    }



                    function returnTimeDiff() { 

                        time += 1000;  
                        elapsed = Math.floor(time / 1000) / 10;  
                        if(Math.round(elapsed) == elapsed) { elapsed += '.0'; }  
                        return (new Date().getTime() - start) - time; 
                    }


                    return { 

                        callBack: function n () {

                            if (future) { 
                                future.clear();
                            }
                            var diff = returnTimeDiff();
                            var oldSum = round(sum); 
                            calculateSum();
                            seconds = seconds + 1;

                            future = utils.updateNumbers(oldSum, sum, function(sum_) {
                                $("#digits").html(round(sum_) + "e");
                            });

                            $("#time").html(utils.parseTime(seconds));

                            utils.checkTreshold(sum, function () { 

                                if (sound === 1) { 
                                    $("#ching")[0].play();

                                }
                            });
                            timer = setTimeout(function() {n();}, 1000-diff);
                        },

                        reset: function() { 

                            sum = 0;
                            seconds = 0;
                            time = 0;
                            utils.checkTreshold(0).reset();
                            $("#digits").html("0");
                            $("#time").html("0h 0m 0s");
                            $("#configuration").show("slow");
                        },

                        stop: function() { 

                            future.clear();
                            if (timer) { 

                                clearTimeout(timer);
                                timer = undefined;
                                time = 0;
                                elapsed = '0.0'; 
                            }
                            $("#configuration").show("slow");
                        },

                        start: function() {
                            if (!timer) {
                                start = new Date().getTime();  
                                ns.callBack();

                            }
                            $("#configuration").hide("slow");
                            printSlogan();
                        },

                        init: function() {

                            printSlogan();

                            $("#start").click(function() { ns.buthandler(this); ns.start(); });
                            $("#stop").click(function() { ns.buthandler(this);ns.stop(); });
                            $("#reset").click(function() { ns.buthandler(this); ns.reset(); });
                            $("#showcontrol").click(function() { $("#configuration").toggle("slow");});
                            $("#togglesound").click(function() { ns.toggleSound(); });
                        },

                        buthandler: function(element) { 

                            $("#controls").children().removeClass("pushed");
                            $(element).addClass("pushed");
                        },

                        toggleSound: function() { 
                            if (sound === 0) { 
                                sound = 1;
                                $("#S").html("S");
                            }
                            else { 
                                sound = 0;
                                $("#S").html("M");
                            }

                        }
                    };


                })(jQuery, window.utils);
