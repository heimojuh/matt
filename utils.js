window.utils = (function($) {
    "use strict";

    var clr;

    var updateNumber = function u(oldSum, sum, cb) {

        if (oldSum >= sum) {
            return;
        }
        cb(oldSum);


        clr = setTimeout(function() {u(oldSum+0.01, sum, cb);}, 30);
    };

    updateNumber.clear = function() { 
        clearTimeout(clr);
    };

    var treshold = function(against,cb) { 

        if(against > treshold.c) { 
            cb(); 
            treshold.c = treshold.c + treshold.t;
        }

    };
    treshold.t = 100;
    treshold.c = 100;
    treshold.reset = function() { 
        treshold.t = 100;
        treshold.c = 100;
    };


    function formatTime (secs) { 
        var hours = Math.floor(secs / (60 * 60));

        var divisor_for_minutes = secs % (60 * 60);
        var minutes = Math.floor(divisor_for_minutes / 60);

        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil(divisor_for_seconds);

        var obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;

    }

    return { 

        updateNumbers: function(oldSum, sum, cb) { 

            updateNumber(oldSum, sum, cb);
            return updateNumber;

        },
        checkTreshold: function(against,cb) {

            treshold(against, cb);
            return treshold;

        },

        parseTime: function(seconds) { 
            var t = formatTime(seconds);
            return "" + t.h +"h "+t.m+"m "+t.s+"s";
        }






    };

})();
